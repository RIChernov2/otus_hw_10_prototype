﻿using Prototype.AbstractFactories.Interfaces;
using Prototype.Entities;
using Prototype.Entities.AbstractClasses;
using Prototype.Enums;

namespace Prototype.AbstractFactories
{
    /// <summary>
    /// Фабрика, создающая комплектующие телефона IPnone4s
    /// </summary>
    public class Phone4sPackFactory: IPnonePackFactory
    {
        public BaseIPhone CreatePhone() => new Phone4s();
        public Charger CreateCharger() 
            => new Charger()
            .SetAdapter(new Adapter(AdapterAmperage.PointFiveAmpere))
            .SetCable(new Cable(CableConnection.DXP30Pin));
        public EarPods CreateEarPods()
            => new EarPods(EarPodsConnection.MiniJack);
    }
}
