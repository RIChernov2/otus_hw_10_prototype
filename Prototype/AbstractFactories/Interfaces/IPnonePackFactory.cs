﻿using Prototype.Entities;
using Prototype.Entities.AbstractClasses;

namespace Prototype.AbstractFactories.Interfaces
{
    /// <summary>
    /// Интерфейс абстрактной фабрики, создающей комплектующие телефонов
    /// </summary
    public interface IPnonePackFactory
    {
        BaseIPhone CreatePhone();
        Charger CreateCharger();
        EarPods CreateEarPods();
    }
}
