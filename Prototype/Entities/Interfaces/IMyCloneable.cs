﻿
namespace Prototype.Entities.Interfaces
{
    public interface IMyCloneable<T>
    {
        T MyClone();
    }
}
