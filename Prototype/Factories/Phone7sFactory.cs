﻿using Prototype.Entities;
using Prototype.Factories.Interfaces;
using Prototype.Entities.AbstractClasses;

namespace Prototype.Factories
{
    /// <summary>
    /// Фабрика, создающая телефон IPnone7s
    /// </summary>
    public class Phone7sFactory : IPhoneFactory
    {
        public BaseIPhone CreatePhone() => new Phone7s();
    }
}
