﻿using Prototype.Entities.AbstractClasses;

namespace Prototype.Factories.Interfaces
{
    /// <summary>
    /// Интерфейс фабрики, создающей телефон
    /// </summary>
    public interface IPhoneFactory
    {
        public BaseIPhone CreatePhone();
    }
}
