﻿using Prototype.Entities;
using Prototype.Factories.Interfaces;
using Prototype.Entities.AbstractClasses;

namespace Prototype.Factories
{
    /// <summary>
    /// Фабрика, создающая телефон IPnone4s
    /// </summary>
    public class Phone4sFactory : IPhoneFactory
    {
        public BaseIPhone CreatePhone() => new Phone4s();
    }
}
