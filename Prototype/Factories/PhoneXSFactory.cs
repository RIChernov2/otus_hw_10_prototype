﻿using Prototype.Entities;
using Prototype.Factories.Interfaces;
using Prototype.Entities.AbstractClasses;

namespace Prototype.Factories
{
    /// <summary>
    /// Фабрика, создающая телефон IPnoneXS
    /// </summary>
    public class PhoneXSFactory : IPhoneFactory
    {
        public BaseIPhone CreatePhone() => new PhoneXS();
    }
}
