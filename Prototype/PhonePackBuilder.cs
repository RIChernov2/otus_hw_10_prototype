﻿using Prototype.AbstractFactories.Interfaces;
using Prototype.Entities;

namespace Prototype
{
    /// <summary>
    /// Класс, собирающий комплектующие в конечный продукт для продажи
    /// </summary>
    public class PhonePackBuilder
    {
        public PhonePackBuilder(IPnonePackFactory packFactory)
        {
            _packFactory = packFactory;
        }
        IPnonePackFactory _packFactory;

        public PhonePack CreatePhonePack()
        {
            return new PhonePack(_packFactory.CreatePhone(), _packFactory.CreateCharger(), _packFactory.CreateEarPods());
        }

    }
}
